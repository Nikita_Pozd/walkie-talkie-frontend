# Установка

Для запуска на ПК должны быть установлены:
[Node.js](https://nodejs.org/);
[Yarn](https://yarnpkg.com/);
[Git](https://git-scm.com/);

Склонируйте репозиторий

```sh
git clone https://gitlab.com/Nikita_Pozd/walkie-talkie-frontend.git
```

Из корня проекта сделайте установку необходимых зависимостей

```sh
yarn install
```

Убедитесь, что в редакторе (если у вас VS Code) установлены:
[Prettier](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode);
[ESlint](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint)

Запустите проект

```sh
yarn dev
```
